(function($) {

    "use strict";

    var $html = $("html");

    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

    var global_functions = {
        init: function() {
            var self = this;

            self.onMobile();
            self.echarts_init();
            self.toggle_sidebar();
            // self.tab();
            self.data_table();
            self.dropdown();
            self.nice_scroll();
        }
        ,
        onMobile: function() {
            
        }
        ,
        //=include partials/_echarts_init.js
        ,
        toggle_sidebar: function() {
            $('.nav--toggle').on('click', function (e) {
                e.preventDefault();

                $('.sidebar').toggleClass('minimized');
            });
        }
        ,
        tab: function() {
            $(document).on('click', '[data-toggle="tab"]', function(evt) {
                var tab_id = $(this).data('target');
                evt.preventDefault();
                evt.stopPropagation();

                $('.tab--nav li').removeClass('is-active');
                $('.tab--pane').removeClass('is-active');

                $(this).parent().addClass('is-active');
                $(tab_id).addClass('is-active');
            });
        }
        ,
        dropdown: function() {
            $(document).on('click', '[data-toggle="dropdown"]', function(evt) {
                evt.preventDefault();
                evt.stopPropagation();
                $('[data-toggle="dropdown"]').not(this).removeClass('dropdown-active');
                $(this).toggleClass('dropdown-active');
            });

            $(document).on('click', '.dropdown-menu', function(evt) {
                evt.preventDefault();
                evt.stopPropagation();
            });

            $(document).on('click', function(evt) {
                if (!$(evt.target).closest('[data-toggle="dropdown"]').length) {
                    $('[data-toggle="dropdown"]').removeClass('dropdown-active');
                }
            });

            $(document).on('keydown', function(evt) {
                var dropdown;
                var evt = evt || $(window).event;
                dropdown = $('[data-toggle="dropdown"]');
                if (evt.keyCode === 27) {
                    dropdown.removeClass('dropdown-active');
                }
            });

            $('.dropdown-menu li a').on('click', function(evt) {
                evt.stopPropagation();
            });

            $('.dropdown-menu li a input').on('click', function(evt) {
                evt.stopPropagation();
            });
        }
        ,
        data_table: function() {
            $('.table--with-datatable').DataTable({
                autoWidth: false,
                responsive: true,
                searching: false,
                info: false,
                paging: false
            });
        }
        ,
        nice_scroll: function() {
            $(".scrollable").niceScroll({
                mousescrollstep: 100,
                cursorcolor: '#ccc',
                cursorborder: '',
                cursorwidth: 3,
                hidecursordelay: 100,
                autohidemode: 'scroll',
                horizrailenabled: false,
                preservenativescrolling: false,
                railpadding: {
                    right: 0.5,
                    top: 1.5,
                    bottom: 1.5
                }
            });
        }
    };

    $(document).ready(function() {
        global_functions.init();
    });

})(jQuery);
