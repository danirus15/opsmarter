echarts_init: function() {
    var basicChart = document.getElementById('basicChart');

	if (basicChart !== null && basicChart.length !== 0) {
        var basic_chart = echarts.init(basicChart);

        var basic_chart_options = {
            grid: {
                x: 40,
                y: 35,
                x2: 20,
                y2: 25
            },
            tooltip: {
                trigger: 'axis',
                backgroundColor: '#051839',
                padding: [10, 15, 10, 15]
            },
            xAxis: [{
                    type: 'category',
                    boundaryGap: false,
                    data: [ '1 Dec', '5 Dec', '10 Dec', '15 Dec', '20 Dec', '25 Dec', '31 Dec' ]
            }],
            yAxis: [{
                type: 'value'
            }],
            color: [ 
                '#44E0B4', '#6F3CBC', '#F12B47'
            ],
            series: [
                {
                    name: 'x',
                    type: 'line',
                    smooth: true,
                    itemStyle: {normal: {areaStyle: {type: 'default'}}},
                    data: [10, 12, 21, 54, 260, 830, 710]
                },
                {
                    name: 'y',
                    type: 'line',
                    smooth: true,
                    itemStyle: {normal: {areaStyle: {type: 'default'}}},
                    data: [30, 182, 434, 791, 390, 30, 10]
                },
                {
                    name: 'z',
                    type: 'line',
                    smooth: true,
                    itemStyle: {normal: {areaStyle: {type: 'default'}}},
                    data: [1320, 1132, 601, 234, 120, 90, 20]
                }
            ]
        };

        basic_chart.setOption(basic_chart_options);

        window.onresize = function () {
            setTimeout(function () {
                basic_chart.resize();
            }, 200);
        }
    }
}