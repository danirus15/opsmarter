var gulp = require('gulp');
var rimraf = require('rimraf');
var config = require('../config');


gulp.task('watch', [
	'html:watch',
	'scss:watch',
	'images:watch',
	'js:watch',
	'copy:watch',
	'svgsprite:watch'
]);

gulp.task('delete', function (cb) {
    rimraf(config.dist.root, cb);
});

gulp.task('default', ['watch', 'html', 'scss', 'images', 'js-libs', 'js-app', 'copy', 'svgsprite'], function() {});
