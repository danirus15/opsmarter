var gulp = require('gulp');
var notify = require('gulp-notify');
var imagemin = require('gulp-imagemin');
var config = require('../config');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

var imagePath = config.src.images + '**/*';


gulp.task('images', function() {
    return gulp.src(imagePath)
    	.pipe(imagemin({ optimizationLevel: 5 }))
        .pipe(gulp.dest(config.dist.images))
        .pipe(reload({ stream: true }))
        .pipe(notify('Images updated!'));
});

gulp.task('images:watch', function() {
    gulp.watch(config.src.images + '**/*', ['images']);
});