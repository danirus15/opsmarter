var gulp = require('gulp');
var include = require('gulp-include');
var notify = require('gulp-notify');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var config = require('../config');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;


gulp.task('js-libs', function() {
    gulp.src(config.src.js + 'libs.js')
        .pipe(include())
        .on('error', function() { notify("Javascript libs include error!"); })
        .pipe(concat('libs.js'))
        .pipe(sourcemaps.init())
        .pipe(gulp.dest(config.dist.js))
        .pipe(reload({ stream: true }))
        .pipe(notify('Javascript libs updated!'));
});

gulp.task('js-app', function() {
    gulp.src(config.src.js + 'partials.js')
        .pipe(include())
        .on('error', function() { notify("Javascript app include error!"); })
        .pipe(concat('app.js'))
        .pipe(sourcemaps.init())
        .pipe(gulp.dest(config.dist.js))
        .pipe(reload({ stream: true }))
        .pipe(notify('Javascript app updated!'));
});

gulp.task('js:watch', function() {
    gulp.watch(config.src.js + 'libs.js', ['js-libs']);
    gulp.watch(config.src.js + 'partials.js', ['js-app']);
});
