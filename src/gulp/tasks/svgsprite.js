var gulp = require('gulp');
var notify = require('gulp-notify');
var svgSprite = require('gulp-svg-sprite');
var config = require('../config');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

var svgConfig = {
    shape : {
        dimension : {
            maxWidth : 32,
            maxHeight : 32
        }
    },
    mode : {
        symbol : true
    }
};


gulp.task('svgsprite',function(){
    return gulp.src(config.src.images + '/iconfont/**/*.svg')
    	.pipe(svgSprite(svgConfig))
    	.pipe(gulp.dest(config.dist.images))
    	.pipe(reload({ stream: true }))
        .pipe(notify('SVG Sprite created!'));
});

gulp.task('svgsprite:watch', function() {
    gulp.watch(config.src.images + '/iconfont/**/*.svg', ['svgsprite']);
});