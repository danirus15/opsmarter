var gulp = require('gulp');
var notify = require('gulp-notify');
var config = require('../config');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;


gulp.task('copy', function() {
    gulp.src(config.src.fonts + '*.*')
        .pipe(gulp.dest(config.dist.fonts))
        .pipe(reload({ stream: true }))
        .pipe(notify('Files copied!'));
});

gulp.task('copy:watch', function() {
    gulp.watch(config.src.fonts + '*', ['copy']);
});
