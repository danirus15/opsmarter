var gulp = require('gulp');
var include = require('gulp-include');
var notify = require('gulp-notify');
var config = require('../config');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;


gulp.task('html', function() {
    gulp.src(config.src.root + '*.html')
    	.pipe(include())
    	.on('error', function() { notify("HTML include error!"); })
    	.pipe(gulp.dest(config.dist.root))
        .pipe(reload({ stream: true }))
        .pipe(notify('HTML updated!'));
});

gulp.task('html:watch', function() {
    gulp.watch([
    	config.src.root + '*.html',
    	config.src.root + 'includes/*.html'
    ], ['html']);
});
