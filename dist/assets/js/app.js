(function($) {

    "use strict";

    var $html = $("html");

    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

    var global_functions = {
        init: function() {
            var self = this;

            self.onMobile();
            self.echarts_init();
            self.toggle_sidebar();
            // self.tab();
            self.data_table();
            self.dropdown();
            self.nice_scroll();
        }
        ,
        onMobile: function() {
            
        }
        ,
        echarts_init: function() {
            var basicChart = document.getElementById('basicChart');
        
        	if (basicChart !== null && basicChart.length !== 0) {
                var basic_chart = echarts.init(basicChart);
        
                var basic_chart_options = {
                    grid: {
                        x: 40,
                        y: 35,
                        x2: 20,
                        y2: 25
                    },
                    tooltip: {
                        trigger: 'axis',
                        backgroundColor: '#051839',
                        padding: [10, 15, 10, 15]
                    },
                    xAxis: [{
                            type: 'category',
                            boundaryGap: false,
                            data: [ '1 Dec', '5 Dec', '10 Dec', '15 Dec', '20 Dec', '25 Dec', '31 Dec' ]
                    }],
                    yAxis: [{
                        type: 'value'
                    }],
                    color: [ 
                        '#44E0B4', '#6F3CBC', '#F12B47'
                    ],
                    series: [
                        {
                            name: 'x',
                            type: 'line',
                            smooth: true,
                            itemStyle: {normal: {areaStyle: {type: 'default'}}},
                            data: [10, 12, 21, 54, 260, 830, 710]
                        },
                        {
                            name: 'y',
                            type: 'line',
                            smooth: true,
                            itemStyle: {normal: {areaStyle: {type: 'default'}}},
                            data: [30, 182, 434, 791, 390, 30, 10]
                        },
                        {
                            name: 'z',
                            type: 'line',
                            smooth: true,
                            itemStyle: {normal: {areaStyle: {type: 'default'}}},
                            data: [1320, 1132, 601, 234, 120, 90, 20]
                        }
                    ]
                };
        
                basic_chart.setOption(basic_chart_options);
        
                window.onresize = function () {
                    setTimeout(function () {
                        basic_chart.resize();
                    }, 200);
                }
            }
        }
        ,
        toggle_sidebar: function() {
            $('.nav--toggle').on('click', function (e) {
                e.preventDefault();

                $('.sidebar').toggleClass('minimized');
            });
        }
        ,
        tab: function() {
            $(document).on('click', '[data-toggle="tab"]', function(evt) {
                var tab_id = $(this).data('target');
                evt.preventDefault();
                evt.stopPropagation();

                $('.tab--nav li').removeClass('is-active');
                $('.tab--pane').removeClass('is-active');

                $(this).parent().addClass('is-active');
                $(tab_id).addClass('is-active');
            });
        }
        ,
        dropdown: function() {
            $(document).on('click', '[data-toggle="dropdown"]', function(evt) {
                evt.preventDefault();
                evt.stopPropagation();
                $('[data-toggle="dropdown"]').not(this).removeClass('dropdown-active');
                $(this).toggleClass('dropdown-active');
            });

            $(document).on('click', '.dropdown-menu', function(evt) {
                evt.preventDefault();
                evt.stopPropagation();
            });

            $(document).on('click', function(evt) {
                if (!$(evt.target).closest('[data-toggle="dropdown"]').length) {
                    $('[data-toggle="dropdown"]').removeClass('dropdown-active');
                }
            });

            $(document).on('keydown', function(evt) {
                var dropdown;
                var evt = evt || $(window).event;
                dropdown = $('[data-toggle="dropdown"]');
                if (evt.keyCode === 27) {
                    dropdown.removeClass('dropdown-active');
                }
            });

            $('.dropdown-menu li a').on('click', function(evt) {
                evt.stopPropagation();
            });

            $('.dropdown-menu li a input').on('click', function(evt) {
                evt.stopPropagation();
            });
        }
        ,
        data_table: function() {
            $('.table--with-datatable').DataTable({
                autoWidth: false,
                responsive: true,
                searching: false,
                info: false,
                paging: false
            });
        }
        ,
        nice_scroll: function() {
            $(".scrollable").niceScroll({
                mousescrollstep: 100,
                cursorcolor: '#ccc',
                cursorborder: '',
                cursorwidth: 3,
                hidecursordelay: 100,
                autohidemode: 'scroll',
                horizrailenabled: false,
                preservenativescrolling: false,
                railpadding: {
                    right: 0.5,
                    top: 1.5,
                    bottom: 1.5
                }
            });
        }
    };

    $(document).ready(function() {
        global_functions.init();
    });

})(jQuery);
